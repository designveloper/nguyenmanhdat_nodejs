### 3. Node Core
* Global variables stored in modules, not in global object as in browser.
* process.argv.indexOf(flag)
* process.std.out, process.std.on('data', cb), process.on('exit', cb)
* process.std.clearLine, process.std.cursorTo


### 4. Core modules
* util.log  = console.log + timestamp
* readline 

        :::javascript
        rl = readline.createInterface(process.stdin, process.stdout)
        rl.question(str, cb)
        rl.setPrompt(str, cb)
        rl.on('line', cb)
        rl.on('close', cb)

* util.inherits(c1, c2)
* child process exec

        :::javascript
        var exec = require("child_process").exec
        exec("git version", function(err, stdout) {...}


### 5. The File System
* `fs` module
    * `readdir` listing directory
    * `readFile`, `readFileSync`
    * `statSync`
    * `writeFile`, `writeFileSync`
    * `existsSync`
    * `mkdir`
    * `rename`
    * `unlink` remove file
    * `rmdir`
* `statSync`

        :::javascript
        var stats = fs.statSync(file)
        if (stats.isFile()) {}

* Stream

        :::javascript
        var stream = fs.createReadStream("./chat.log", "UTF-8")
        var data = ""
        stream.once("data", function() {})
        stream.on("data", function(chunk) {
          data += chunk
        }) 
        stream.on("end", function() {})


### 6. The HTTP module
* Making a request 

        :::javascript
        var options = {
	  hostname: "en.wikipedia.org",
	  port: 443,
	  path: "/wiki/George_Washington",
	  method: "GET"
        }
        var req = https.request(options, function(res) {})

* Serving files

        :::javascript
        http.createServer(function(req, res) {
        if (req.url.match(/.css$/)) {
	  var cssPath = path.join(__dirname, 'public', req.url)
	  var fileStream = fs.createReadStream(cssPath, "UTF-8")
	  res.writeHead(200, {"Content-Type": "text/css"})
	  fileStream.pipe(res)
	}